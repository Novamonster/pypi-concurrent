#!/usr/bin/env python
# -*- coding: utf-8 -*-

##Imports
import requests as rq
from bs4 import BeautifulSoup as bs
import pandas as pd
import concurrent.futures
from queue import Queue
from threading import Lock

class PypiScraperC(object):
    def __init__(self, keyword, *args, **kwargs):
        super(PypiScraperC, self).__init__()
        self.keyword = keyword
        self.flag = 3
        self.pagenum = 1
        self.last_page = 0
        self.hl = "https://pypi.org/search/?q=&o="
        self.framework = f'https://pypi.org/search/?q=&o=&c={self.keyword}+%3A%3A+'
        self.category_links = ['https://pypi.org/search/?q=&o=&c=Framework+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=Topic+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=Development+Status+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=License+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=Programming+Language+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=Operating+System+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=Environment+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=Intended+Audience+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=Natural+Language+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=Typing+%3A%3A+']
        self.page_tail= '&o=&q=&page=1'
        self.list_of_categories= ["Framework", 'Topic', 'Development_Status', 'License', 'Programming_Language', 'Operating_System', 'Environment', 'Intended_Audience', 'Natural_Language', 'Typing']
        self.list_of_checkbox = []
        self.all_the_projects = Queue()
        self.alltheinfo = Queue()
        self.listoflinks = []
        self.getLinks()
        self.disabledRacoon()
        self.getPages()
        self.saveStuff()

    '''O'''
    def parse_url(self, url: str) -> object:
        '''Takes a string and parses it to check if it exists or not
        if it does it returns a bs object'''
        r = rq.get(url)
        if r.status_code==200:
            page = r.text
            return bs(page, "lxml")
        else:
            pass
           
    '''O'''
    def lookForName(self, project: object) -> str:
        '''Takes in an link to an specific plugin in the https://pypi.org/ site
        and gets the name of the plugin'''
        try:
            self.name = project.find('h1')
            return self.name.text.strip().split(' ')[0]
        except:
            return "No Name"

    '''O'''
    def lookForVersion(self, project: object) -> str:
        '''Takes in an link to an specific plugin in the https://pypi.org/ site
        and gets the version of the plugin'''
        try:
            self.version = project.find('h1')
            return self.version.text.strip().split(' ')[1]
        except:
            return "No Version"
   
    '''O'''    
    def lookForPip(self, project: object) -> str:
        '''Takes in an link to an specific plugin in the https://pypi.org/ site
        and gets the pip install of the plugin'''
        try:
            self.pip = project.find('p', {'class': 'package-header__pip-instructions'})
            return self.pip.text.split('\n')[1].strip()
        except:
            return "No PIP"
       
    '''O'''
    def lookForDates(self, project: object) -> str:
        '''Takes in an link to an specific plugin in the https://pypi.org/ site
        and gets all the times the plugin has been updated'''
        try:
            self.dates_as_list = project.findAll('p', {'class': 'release__version-date'})
            self.dates = [date_listed.text.replace(",", ";").strip() for date_listed in self.dates_as_list]
            self.dates_as_string = ", ".join(self.dates)
            return self.dates_as_string
        except:
            return "No Dates"
   
    '''O'''
    def lookForDateOfRelease(self, project: object) -> str:
        '''returns the date the plugin was added to the site from the variable
        in the lookforDates function'''
        try:
            self.dates_as_list = project.findAll('p', {'class': 'release__version-date'})
            self.release_date = self.dates_as_list[-1].text.strip()
            return self.release_date
        except:
            return 'No Release'

    '''O'''
    def lookForHome(self, project: object) -> str:
        '''Takes in an link to an specific plugin in the https://pypi.org/ site
        and gets the home page link of the author of the plugin'''
        try:
            self.home = project.find('a', {'class': "vertical-tabs__tab vertical-tabs__tab--with-icon vertical-tabs__tab--condensed"})
            return self.home["href"]
        except:
            return "No Homepage"
       
    '''O'''
    def lookForLicense(self, project: object) -> str:
        '''Takes in an link to an specific plugin in the https://pypi.org/ site
        and gets the licnese of the plugin'''
        try:
            self.license = project.findAll('p')
            for license_listed in self.license:
                if license_listed.text.split(" ")[0]=="License:":
                    return license_listed.text[9:]
        except:
            return "No License"
           
    '''O'''        
    def lookForAuthor(self, project: object) -> str:
        '''Takes in an link to an specific plugin in the https://pypi.org/ site
        and gets the author of the plugin'''
        try:
            self.author = project.findAll('p')    
            for author_listed in self.author:  
                if author_listed.text.split(" ")[0]=="Author:":
                    return author_listed.text[8:]
        except:
            return "No Author"
           
    '''O(n)'''        
    def getLinks(self):
        '''Looks for all the links/checkboxes within the given accordion id and puts them in a list'''
        self.link = self.parse_url(self.hl)
        self.framework_box =self.link.findAll('div', {'id': f'accordion-{self.keyword}'})[0]
        for check_box in self.framework_box.findAll('li'):
            self.list_of_checkbox.append(check_box.text.strip().split("::"))

            
    '''O'''
    def checkUnder10000(self, url: str) -> bool:
        '''Checks if a L1 checkbox results in over 10,000 and if so returns True'''
        self.url_in_question = self.parse_url(url)
        self.ten_thousand = self.url_in_question.findAll('form', {"action":"/search/"})[2]
        if self.ten_thousand.find('strong').text.strip() == '10,000+':
            self.flag = 0
            return 
        if self.ten_thousand.find('strong').text.strip() == '0': 
            self.flag = 1
            return
        else:
            self.flag = 3
            return
   
    '''O(n^3)'''
    def disabledRacoon(self):
        '''Cleans up the list of the checkboxes by removing the /n/n/n/n and
        exchanging any spaces for + and adds them to a list'''
        self.allurls= []
        for sub_check_box in self.list_of_checkbox:
            for plugin in sub_check_box:
                self.this_framework = plugin.split('\n\n\n\n')
                self.allurls.extend([mapache_descapacitado for mapache_descapacitado in self.Customs(counter = 0)])
       
    '''O(n)'''
    def Customs(self, counter = 0, page = 1) -> bool:
        '''Combines the first item in a list with the general framework link
        then checks if it exsits or it is over the bounds; if it is just over
        the bounds then it takes that link and combines it with the
        next item in the list for every item in the list not including the first
        otherwise, it prints out not real'''

        self.url = self.framework.replace('_', '+')  + str(self.this_framework[0].replace(' ', '+').replace('/', '%2F').replace('(', '%28').replace(')', '%29'))
        if counter > 0:
            self.url += '+%3A%3A+' + self.this_framework[counter]
        counter += 1
        self.checkUnder10000(self.url)
        if self.flag == 0:
            self.over_limit = []
            for sub_checkbox in range(1, len(self.this_framework)):
                self.sub_url = self.url + '+%3A%3A+' + self.this_framework[sub_checkbox].replace(' ', '+').replace('/', '%2F').replace('(', '%28').replace(')', '%29').replace('\n', '+')
                self.over_limit.append(self.sub_url)
                if 'WWW/HTTP' in self.this_framework:
                    self.sub_url2 = self.url + '+%3A%3A+' + 'WWW/HTTP' + '+%3A%3A+' + self.this_framework[sub_checkbox].replace(' ', '+').replace('/', '%2F').replace('(', '%28').replace(')', '%29').replace('\n', '+')
                    print(self.sub_url2, "self.sub_url2")
                    self.over_limit.append(self.sub_url2)
            return self.over_limit
        if self.flag == 1:
            return ['not real']
        else:
            return [self.url]

                
    '''O(n+(O(n*9)^n)'''
    def getPages(self):
        '''Gets the plugin urls from the first page of the links gotten
        from the customs function'''
        for that_url in self.allurls:
            if that_url=="not real":
                print("not real") 
            else:
                thread_manager = []
                cap_page_lock = Lock()
                with concurrent.futures.ThreadPoolExecutor() as executor:
                    for number in range(1, 501):

                        that_url += f'&o=&q=&page={number}'
                        thread_manager.append(executor.submit(self.makeProjects, that_url, cap_page_lock))
                        that_url = that_url.replace(f'&o=&q=&page={number}', '')

                        if self.last_page == 1:
                            self.last_page = 0
                            break
                self.last_page = 0

               
    def makeProjects(self, page_url, cap_page_lock):
        try:
            projectlink = []

            website = self.parse_url(page_url).findAll("span", {'class': "package-snippet__name"})
            incomplete_url = 'https://pypi.org/project/'
            for span in website:
                complete_url = incomplete_url + span.text.strip() + "/"
                projectlink.append(complete_url)

            self.organizer(projectlink)
        except:
            cap_page_lock.acquire()
            self.last_page += 1
            cap_page_lock.release()
            return

    '''O(n*9)'''                
    def organizer(self, projectlink):
        '''Gets the desired information from every link given by
        the makeProjects function from page to rows'''
        for project in projectlink:
            if project not in self.all_the_projects.queue:
                projectlink = self.parse_url(project)
                info = {}
                info['Link to the project'] = project
                info['Name'] = self.lookForName(projectlink)
                info['Version'] = self.lookForVersion(projectlink)
                info['PIP'] = self.lookForPip(projectlink)
                info['Updates'] = self.lookForDates(projectlink)
                info['Home'] = self.lookForHome(projectlink)
                info['Release'] = self.lookForDateOfRelease(projectlink)
                info['License'] = self.lookForLicense(projectlink)
                info['Author'] = self.lookForAuthor(projectlink)
                self.alltheinfo.put(info)
                self.all_the_projects.put(project)
            else:
                continue

            
    '''O'''    
    def saveStuff(self):
        '''Saves all the information given by the organizer function in
        an excel sheet'''
        details = pd.DataFrame.from_records(list(self.alltheinfo.queue))
        details.to_csv('pythonscraper_concurrent.csv', mode='a', index=False)

def main():
    list_of_categories= ["Framework", 'Topic', 'Development_Status', 'License', 'Programming_Language', 'Operating_System', 'Environment', 'Intended_Audience', 'Natural_Language', 'Typing']
    for i in list_of_categories:
        pypi_scraper_concurrent =  PypiScraperC(keyword=i)

if __name__ == '__main__':
    main()